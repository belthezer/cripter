/**
 * Created with IntelliJ IDEA.
 * User: tkarpinski
 * Date: 25.06.13
 * Time: 22:09
 * To change this template use File | Settings | File Templates.
 */

class Cipters {
    public static  _e_caesar(text, val){
        def i=0; def result=""; text.collect{if(i<val.size()){for(def j in 0..<val[i])it++;i++}else{i=0;for(def j in 0..<val[i])it++};result+=it}; result
    }
    public static  _d_caesar(text, val){
        def i=0; def result=""; text.collect{if(i<val.size()){for(def j in 0..<val[i])it--;i++}else{i=0;for(def j in 0..<val[i])it--};result+=it}; result
    }
    public static  _d_matrix(text, val){
        def j=0;def i=0;def result="";
        while(i!=-1) {
            String[][]matrix=new String[val[j*2]][val[j*2+1]]
            for(x in 0..<val[j*2])for(y in 0..<val[j*2+1])if(i<text.size()){matrix[x][y]=text[i];i++}else{matrix[x][y]="";i++}
            for(y in 0..<val[j*2+1])for(x in 0..<val[j*2])result+=matrix[x][y]
            if(i>text.size())i=-1; j++; if(j>=val.size()/2)j=0
        }
        result
    }
    public static  _e_matrix(text, val){
        def j=0;def i=0;def result="";
        while(i!=-1) {
            String[][]matrix=new String[val[j*2]][val[j*2+1]]
            for(y in 0..<val[j*2+1])for(x in 0..<val[j*2])if(i<text.size()){matrix[x][y]=text[i];i++}else{matrix[x][y]="";i++}
            for(x in 0..<val[j*2])for(y in 0..<val[j*2+1])result+=matrix[x][y]
            if(i>text.size())i=-1; j++; if(j>=val.size()/2)j=0
        }
        result
    }
}
