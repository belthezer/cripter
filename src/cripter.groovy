/**
 * Created with IntelliJ IDEA.
 * User: tkarpinski
 * Date: 25.06.13
 * Time: 20:46
 * To change this template use File | Settings | File Templates.
 */
class cripter {
    private static def arguments = ["encript":false,"decript":false]
    private static ALLOWED_CIPHERS = ["CAESAR","MATRIX"]
    private static values = []
    private static cipher_name = ""
    private static source_path = ""
    private static save_path = ""
    private static source = ""
    private static HELP_CODE0 = "HELP!!!"
    private static ERROR_CODE0 = "ERROR: You cant encript and decript in this same time!"
    private static ERROR_CODE1 = {println "ERROR: No cipher named: \"$it\" found."}
    private static ERROR_CODE2 = "ERROR: You must specify cipher name after \"-c\"."
    private static ERROR_CODE3 = "ERROR: You must specify source path after \"-s\"."
    private static ERROR_CODE4 = {println "ERROR: No file under: \"$it\" path found."}
    private static ERROR_CODE5 = "ERROR: I don't know if should I encript or decript. See in cripter -help."
    private static ERROR_CODE6 = "ERROR: You must specify value/s after \"-v\"."
    private static ERROR_CODE7 = {println "ERROR: \"$it\" is not a number."}
    private static ERROR_CODE8 = "ERROR: Not enough values. Should be x*2."
    private static ERROR_CODE9 = "ERROR: You must specify value/s after \"-o\"."
    private static ERROR_CODE10 = "ERROR: You must specify any source, example -s file.txt or -s \"my text\""
    private static ERROR_CODE11 = "ERROR: You must specify any cipher, example -c matrix or -c caesar. Allowed: "+ALLOWED_CIPHERS
    private static ERROR_CODE12 = "ERROR: Specify values, example -v 1 2 3 4"

    public static void main(String[] args) {
        println args
        if ("-help" in args) {
            println HELP_CODE0
            return
        } else {
            if ("-e" in args) arguments.encript=true
            if ("-d" in args) arguments.decript=true
            if ("-c" in args) {
                try {cipher_name = args.getAt(args.findIndexOf {it=="-c"}+1).toString().toUpperCase()} catch(Exception exc) {println ERROR_CODE2; return}
                if (!(cipher_name in ALLOWED_CIPHERS)){cipher_name.eachLine(ERROR_CODE1); return}
            }
            if ("-s" in args) {
                try {source_path = args.getAt(args.findIndexOf {it=="-s"}+1)} catch(Exception exc) {println ERROR_CODE3; return}
                if (source_path.findIndexOf{it==" "}!=-1) {source=source_path;source_path=""}
                else try {def myFile=new File(source_path); def printFileLine={}; myFile.eachLine(printFileLine)} catch(Exception exc) {source_path.toString().eachLine(ERROR_CODE4); return}
            }
            if ("-v" in args) {
                try {args.getAt(args.findIndexOf {it=="-v"}+1)} catch(Exception exc) {println ERROR_CODE6; return}
                def ctn=true; for (def i in args.findIndexOf {it=="-v"}+1..<args.length)if (args[i].take(1)!="-"&&ctn==true)if(args[i].toString().isNumber())values.add(args[i].toInteger())else {args[i].toString().eachLine(ERROR_CODE7);return}else ctn=false
            }
            if ("-o" in args) {
                try {save_path=args.getAt(args.findIndexOf {it=="-o"}+1)} catch(Exception exc) {println ERROR_CODE9; return}
            }
        }
        def i = 0; arguments.eachWithIndex { Map.Entry<String, Boolean> entry, int j -> if (entry.value==true)i++}; if (i>1) {println ERROR_CODE0;return} else if (i==0) {println ERROR_CODE5; return}
        _start()
    }
    private static void _start(){
        if (source_path=="" && source==""){println ERROR_CODE10; return}
        if (cipher_name==""){println ERROR_CODE11; return}
        if (values.size()==0){println ERROR_CODE12; return}

        def result=""
        if (source_path!="") {def myFile=new File(source_path); def printFileLine={source+=it}; myFile.eachLine(printFileLine)}
        if (arguments.encript && cipher_name=="CAESAR") result=Cipters._e_caesar(source, values)
        if (arguments.decript && cipher_name=="CAESAR") result=Cipters._d_caesar(source, values)

        if (arguments.encript && cipher_name=="MATRIX") {if (values.size()%2!=0){println ERROR_CODE8;return}else result=Cipters._e_matrix(source, values)}
        if (arguments.decript && cipher_name=="MATRIX") {if (values.size()%2!=0){println ERROR_CODE8;return}else result=Cipters._d_matrix(source, values)}

        if (save_path!=""){new File(save_path).delete(); new File(save_path) << result; println"OK"}else println result
    }
}
